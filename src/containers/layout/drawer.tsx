import React from 'react'
import { Trans } from '@lingui/react'
import { withStyles } from '@material-ui/core/styles'
import MUIDrawer from '@material-ui/core/Drawer'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'

interface IProps {
  classes?: any;
  onClose: any;
  open: boolean;
}
class Drawer extends React.Component<IProps> {
  handleClose = () => {
    const { onClose } = this.props

    onClose()
  };

  render() {
    const { classes, open } = this.props

    return (
      <MUIDrawer
        classes={{ paper: classes.paper }}
        open={open}
        onClose={this.handleClose}
      >
        <Toolbar>
          <Typography color="inherit" variant="h6">
            <Trans id="app_title" />
          </Typography>
        </Toolbar>
      </MUIDrawer>
    )
  }
}

const style = () => ({
  paper: {
    maxWidth: '90%',
    width: '300px'
  }
})

export default withStyles(style)(Drawer)
