import React from 'react'
import { Trans } from '@lingui/react'
import { Theme, withStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'
import MenuIcon from '@material-ui/icons/Menu'

interface IProps {
  classes?: any;
  onMenuButtonClick: any;
}

const Header: React.FC<IProps> = ({ classes, onMenuButtonClick }) => {
  return (
    <AppBar position="fixed">
      <Toolbar>
        <IconButton
          aria-label="Menu"
          className={classes.menuButton}
          color="inherit"
          onClick={onMenuButtonClick}
        >
          <MenuIcon />
        </IconButton>
        <Typography color="inherit" variant="h6">
          <Trans id="app_title" />
        </Typography>
      </Toolbar>
    </AppBar>
  )
}

const style = (theme: Theme) => ({
  menuButton: {
    marginRight: theme.spacing.unit * 2
  }
})

export default withStyles(style)(Header)
