import React from 'react'
import styled from 'styled-components'
import { Theme, withStyles } from '@material-ui/core/styles'

import Header from './header'
import Drawer from './drawer'

interface IProps {
  classes?: any
}

interface IState {
  drawerOpen: boolean,
}

class Layout extends React.Component<IProps, IState> {
  state = {
    drawerOpen: false,
  }

  handleDrawerToggle = () => {
    this.setState(({ drawerOpen }) => ({ drawerOpen: !drawerOpen }))
  }

  render() {
    const { classes } = this.props
    const { drawerOpen } = this.state

    return (
      <>
        <Wrapper>
          <Header onMenuButtonClick={this.handleDrawerToggle} />
          <div className={classes.toolbar} />
          <StyledContent />
        </Wrapper>
        <Drawer onClose={this.handleDrawerToggle} open={drawerOpen} />
      </>
    )
  }
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`

const StyledContent = styled.div`
  background-color: whitesmoke;
  flex-grow: 1;
  overflow-y: auto;
`

const style = (theme: Theme) => ({
  toolbar: {
    ...theme.mixins.toolbar,
  },
})

export default withStyles(style)(Layout)