import React from 'react'
import { I18nProvider } from '@lingui/react'

import catalogEn from '../../locales/en/messages'
import Layout from '../layout'

const catalogs = {
  en: catalogEn
}

function App() {
  return (
    <I18nProvider catalogs={catalogs} language="en">
      <Layout />
    </I18nProvider>
  )
}

export default App
